import { AppProps } from 'next/app';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <div>
        <Component {...pageProps} />
      </div>
    </>
  );
}

export default CustomApp;
