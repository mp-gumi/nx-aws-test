import { GetServerSideProps } from 'next';

export type PagesProps = {
  first: string;
};

export function Pages({ first }: PagesProps) {
  return <div>{first}</div>;
}

export const getServerSideProps: GetServerSideProps<PagesProps> = async () => {
  return {
    props: {
      first: 'hello',
    },
  };
};

export default Pages;
